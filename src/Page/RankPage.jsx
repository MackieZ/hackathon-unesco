import React, { Component } from 'react'
import './RankPage.scss'
import * as firebase from 'firebase'
import NavbarComponent from '../Component/navbar'
import RedirectPage from './RedirectPage'
import QRCode from 'qrcode.react'
import moment from 'moment'
import Button from '../Component/his-button'

class RankPage extends Component {
  state = {
    timer: 0,
    trigger: true,
    students: {},
    studentsRank: []
  }

  constructor() {
    super()
    this.counter = 0
  }

  componentDidMount() {
    firebase
      .database()
      .ref('rooms/' + this.props.location.state.code)
      .on('value', snap => {
        const val = snap.val()
        const timeEnd = moment.unix(val.timeEnd)
        const duration = moment.duration(timeEnd.diff(moment()))

        this.setState(
          {
            timer: ~~duration.asSeconds() >= 0 ? ~~duration.asSeconds() : 0,
            ...val
          },
          () => this.state.trigger && this.timeCount()
        )
      })

    firebase
      .database()
      .ref('rooms/' + this.props.location.state.code + '/students')
      .on('value', snap => {
        let students = snap.val()
        students &&
          Object.keys(students).sort((a, b) => {
            return b.score - a.score
          })

        let studentsRank = []

        students &&
          Object.keys(students).map((student, index) => {
            students[student].rank = index + 1
            studentsRank[index] = { ...students[student] }
          })

        this.setState({ students: students || {}, studentsRank }, () => {
          firebase
            .database()
            .ref('rooms/' + this.props.location.state.code + '/students')
            .set(students)
        })
      })
  }

  timeCount = () => {
    this.setState(
      { trigger: false },
      () => (this.counter = setInterval(this.countDown, 1000))
    )
  }

  countDown = () => {
    const sec = this.state.timer - 1
    if (sec <= 0) {
      clearInterval(this.counter)
      this.setState(
        {
          timer: 0
        },
        () => {
          firebase
            .database()
            .ref('rooms/' + this.props.location.state.code + '/status')
            .set('done')
        }
      )
    } else {
      this.setState({
        timer: sec
      })
    }
  }

  handlerTimeStop = () => {
    const { code } = this.props.location.state
    firebase
      .database()
      .ref('rooms/' + code + '/status')
      .set('done')
      .then(() => {
        firebase
          .database()
          .ref('rooms/' + code)
          .once('value', snap => {
            const timeEnd = moment().unix('X')
            firebase
              .database()
              .ref('rooms/' + code + '/timeEnd')
              .set(timeEnd)
              .then(() => {
                this.setState({ timer: 0 })
              })
          })
      })
  }

  handlerScore = () => {
    this.props.history.push({
      pathname: 'score',
      state: {
        code: this.props.location.state.code
      }
    })
  }

  render() {
    const { code, name, description, studentsRank, timer } = this.state
    return (
      <div id="rank">
        <NavbarComponent />
        <RedirectPage />
        <div
          id="qrcode"
          className="row d-flex justify-content-center p-3 m-0 fadein"
        >
          <div className="row qr d-flex m-0 p-2 justify-content-center align-items-center">
            <QRCode value={this.props.location.state.code} size={128} />
            <div className="timer">
              {`${
                ~~(timer / 60) > 9 ? `${~~(timer / 60)}` : `0${~~(timer / 60)}`
              } : ${
                ~~(timer % 60) <= 9 ? `0${~~(timer % 60)}` : ~~(timer % 60)
              } `}
              {timer !== 0 ? (
                <button
                  onClick={() => this.handlerTimeStop()}
                  className="stop-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                >
                  หยุดเวลา
                </button>
              ) : (
                <Button
                  onClick={() => this.handlerScore()}
                  color="color-green"
                  margin="0 0 0 12px"
                  width="120px"
                >
                  ดูรายละเอียด
                </Button>
              )}
            </div>
          </div>
          <div className="unesco--box--bottom">
            <div className="rank qr--detail unesco--box text-center">
              <div className="qr--room">
                รหัสห้อง : <span className="room">{code}</span>
              </div>
              <div className="room--name">
                {name} ({description}){' '}
              </div>
            </div>
          </div>
          <div className="row col-12 p-0 m-0 d-flex justify-content-center">
            <div className="col-6 justify-content-center p-0 rank--detail d-flex">
              <div className="rank4 d-flex flex-column-reverse">
                <div className="rank--box">4</div>
                <div className="rank--box-detail popup text-nowrap">
                  {studentsRank.length > 0
                    ? studentsRank[3] && studentsRank[3].name
                    : '-'}
                </div>
              </div>
              <div className="rank2 d-flex flex-column-reverse">
                <div className="rank--box">2</div>
                <div className="rank--box-detail popup text-nowrap">
                  {studentsRank.length > 0
                    ? studentsRank[1] && studentsRank[1].name
                    : '-'}
                </div>
              </div>
              <div className="rank1 d-flex flex-column-reverse">
                <div className="rank--box">1</div>
                <div className="rank--box-detail popup text-nowrap">
                  {studentsRank.length > 0
                    ? studentsRank[0] && studentsRank[0].name
                    : '-'}
                </div>
              </div>
              <div className="rank3 d-flex flex-column-reverse">
                <div className="rank--box">3</div>
                <div className="rank--box-detail popup text-nowrap">
                  {studentsRank.length > 0
                    ? studentsRank[2] && studentsRank[2].name
                    : '-'}
                </div>
              </div>
              <div className="rank5 d-flex flex-column-reverse">
                <div className="rank--box">5</div>
                <div className="rank--box-detail popup text-nowrap">
                  {studentsRank.length > 0
                    ? studentsRank[4] && studentsRank[4].name
                    : '-'}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default RankPage
