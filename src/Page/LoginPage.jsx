import React, { Component } from 'react'
import './LoginPage.scss'
import * as firebase from 'firebase'
import logo from '../assets/image/logo-text.png'
import NavbarComponent from '../Component/navbar'
import { Input, message } from 'antd'
import Button from '../Component/his-button'

class LoginPage extends Component {
  state = {
    register: false,
    email: '',
    password: ''
  }
  componentDidMount() {}

  loginHandler = () => {
    firebase
      .auth()
      .signInAnonymously()
      .then(res => {
        this.props.history.push('/dashboard')
      })
  }

  handlerEmail = e => {
    this.setState({ email: e.target.value })
  }

  handlerPassword = e => {
    this.setState({ password: e.target.value })
  }

  handlerStepRegister = () => {
    this.setState({ register: true })
  }

  handlerRegister = e => {
    e.preventDefault()
    const { email, password } = this.state
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.props.history.push('/dashboard')
        message.success('สมัครสมาชิกสำเร็จ')
      })
      .catch(error => {
        message.error(error.message)
      })
  }

  handlerLogin = e => {
    e.preventDefault()
    const { email, password } = this.state
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.props.history.push('/dashboard')
        message.success('เข้าสู่ระบบ')
      })
      .catch(error => {
        message.error(error.message)
      })
  }

  render() {
    const { register, email, password } = this.state
    return (
      <div className="App">
        <NavbarComponent />

        <div className="full-bg d-flex align-items-center justify-content-center">
          <form onSubmit={register ? this.handlerRegister : this.handlerLogin}>
            <div className="login-box d-flex flex-column">
              <img alt="logo" src={logo} />
              <Input
                value={email}
                onChange={this.handlerEmail}
                placeholder="Email"
                className="mt-3"
              />
              <Input
                type="password"
                value={password}
                onChange={this.handlerPassword}
                placeholder="Password"
                className="mt-3"
              />

              {!register ? (
                <div>
                  <Button
                    onClick={this.handlerLogin}
                    width="100%"
                    color="color-red"
                    margin="16px 0"
                  >
                    เข้าสู่ระบบ
                  </Button>
                  <div className="text-center label--or d-flex flex-row justify-content-center">
                    {' '}
                    OR{' '}
                  </div>

                  <Button
                    onClick={this.handlerStepRegister}
                    width="100%"
                    color="color-yellow"
                    margin="16px 0"
                  >
                    สมัครใช้งาน
                  </Button>
                </div>
              ) : (
                <Button
                  onClick={this.handlerRegister}
                  width="100%"
                  color="color-yellow"
                  margin="16px 0"
                >
                  สมัครสมาชิก
                </Button>
              )}
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default LoginPage
