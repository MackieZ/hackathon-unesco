import React, { Component } from 'react'
import NavbarComponent from '../Component/navbar'
import RedirectPage from './RedirectPage'
import { Input, Icon } from 'antd'
import './DocumentPage.scss'
import Button from '../Component/his-button'

class DocumentPage extends Component {
  state = {
    search: '',
    item: [
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/1.png?alt=media&token=43094474-dbee-4591-b36d-5b84cda461d6',
        documentUrl:
          'https://drive.google.com/file/d/1iEhp8JsmFbSd_vvuXSbpxx2xgdnmZ5Fi/view?usp=sharing',
        title: 'ลุ่มน้ำอิระวดี',
        description: 'อาณาจักรพุกาม',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      },
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/2.png?alt=media&token=86d0caa4-f025-4548-829c-53fec88a0f7e',
        documentUrl:
          'https://drive.google.com/file/d/1OCnYzxAv8yiSCplGt4Wyv-Uv-SKSvaG-/view?usp=sharing',
        title: 'พื้นที่ปลูกข้าวบาหลี:',
        description: 'ศาสนาและข้าว',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      },
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/3.png?alt=media&token=2788d9a3-0349-4a6e-b019-a8def5c5041f',
        documentUrl:
          'https://drive.google.com/file/d/1w5oalmwZMX1a3VwFjiqvVKZUbf1KTScU/view?usp=sharing',
        title: 'พื้นที่ภูเขาทางภาคเหนือของไทย',
        description: 'อาณาจักรล้านนา',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      },
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/4.png?alt=media&token=ff7dcba9-23be-4826-8723-e2f66a90ca21',
        documentUrl:
          'https://drive.google.com/file/d/152aiDcgRxr72LMN0K34GZcGn2h5X3rpJ/view?usp=sharing',
        title: 'บ้านเรือนในพื้นที่สูง',
        description: 'Honai and Tongkonan',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      },
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/5.png?alt=media&token=c5531f29-cea3-4378-84ba-a075327e4f5b',
        documentUrl:
          'https://drive.google.com/file/d/1RPFz9P_fdts85Zg4KkG3-Z00ZVv7CMq8/view?usp=sharing',
        title: 'คาบสมุทรมลายูฝั่งตะวันตก',
        description: 'อาณาจักรมะละกา',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      },
      {
        imgUrl:
          'https://firebasestorage.googleapis.com/v0/b/hackathon-unesco-2018.appspot.com/o/6.png?alt=media&token=6360101e-fc28-408f-afb8-083721dae125',
        documentUrl:
          'https://drive.google.com/file/d/1SB1M1D9NzkhE6P0EHxBBzwxsOMxgYs_F/view?usp=sharing',
        title: 'พื้นที่ชายฝั่งสามเหลี่ยมปะการัง',
        description: 'The Sama/Bajau',
        content: `เอกสารประกอบการเรียนการสอน และ รูปภาพประกอบ Augmented Reality (AR)`
      }
    ]
  }

  componentDidMount() {}

  handlerSearchInput = e => {
    this.setState({ search: e.target.value })
  }

  render() {
    const { item } = this.state
    return (
      <div>
        <NavbarComponent activePath={'/download'} />
        <RedirectPage />

        <div id="download" className="row p-3 m-0 fadein">
          <div className="row input--item m-0">
            <Input
              placeholder="ค้นหาเอกสาร"
              prefix={
                <Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />
              }
              value={this.state.search}
              onChange={this.handlerSearchInput}
            />
          </div>

          {item.map((item, index) => {
            if (
              !~item.title.indexOf(this.state.search) &&
              !~item.description.indexOf(this.state.search) &&
              !~item.content.indexOf(this.state.search)
            )
              return

            return (
              <div className="row box--item" key={index}>
                <div className="col-2 p-0 icon">
                  <img
                    style={{ width: '100%', height: '100%' }}
                    src={item.imgUrl}
                    alt="document-photo"
                  />
                </div>
                <div className="col-7 p-4 pr-0">
                  <div className="row m-0 document-title">
                    {item.title}
                    <span className="document-description">
                      ({item.description})
                    </span>
                  </div>
                  <div className="row m-0 mt-3 document-text">
                    {item.content}
                  </div>
                </div>
                <div className="col-3 p-4 d-flex align-items-center justify-content-center flex-column">
                  <a href={item.documentUrl}>
                    <Button
                      color="color-red"
                      margin="10px 0"
                      width="200px"
                      maxWidth="200px"
                    >
                      ดาวน์โหลดเอกสาร
                    </Button>
                  </a>
                  <a href={item.imgUrl}>
                    <Button
                      color="color-purple"
                      margin="10px 0"
                      width="200px"
                      maxWidth="200px"
                    >
                      ดาวน์โหลดรูปภาพ
                    </Button>
                  </a>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

export default DocumentPage
