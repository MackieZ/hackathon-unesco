import React from 'react'
import RedirectPage from './RedirectPage'
import NavbarComponent from '../Component/navbar'
import styled, { keyframes } from 'styled-components'
import './AnswerPage.scss'
import { Tabs } from 'antd'
import * as firebase from 'firebase'
const TabPane = Tabs.TabPane

const Box = styled.div`
  border-left: 10px solid #c6c6c6;
`

const animateRank = keyframes`
  from {
    width: 0px;
    margin-left: -29px;
  }

  to {
    width: ${props => (props.width ? props.width : '50%')};
    margin-left: -29px;
  }
`

const BorderBottom = styled.div`
  border-bottom: 10px solid ${props => (props.color ? '#57e06e' : '#ff7e7b')};
  width: ${props => (props.width ? props.width : '50%')};
  margin-left: -29px;
  animation: ${animateRank} 3.5s;
`

class ScorePage extends React.Component {
  state = {
    room: {
      description: 'Ontime',
      name: 'Loading...',
      questions: [
        {
          answer: 'Loading...',
          choice1: 'Loading...',
          choice2: 'Loading...',
          choice3: 'Loading...',
          questionName: 'Loading...',
          replies: {
            mackie: 'choice1',
            pun: 'answer',
            unun: 'choice2',
            namfon: 'choice1',
            ake: 'choice1'
          }
        },
        {
          answer: 'Loading...',
          choice1: 'Loading...',
          choice2: 'Loading...',
          choice3: 'Loading...',
          questionName: 'Loading...',
          replies: {
            mackie: 'choice1',
            pun: 'answer',
            unun: 'choice2',
            namfon: 'choice1',
            ake: 'choice3'
          }
        }
      ]
    }
  }
  componentDidMount() {
    const roomCode =
      (this.props.location.state && this.props.location.state.code) || '0ykzm4'
    firebase
      .database()
      .ref('rooms/' + roomCode)
      .once('value')
      .then(snapshot => {
        if (snapshot.val()) {
          const room = snapshot.val()
          this.setState({ room }, () => {
            const questions = this.state.room.questions.map(el => {
              el['answerCount'] = 0
              el['choice1Count'] = 0
              el['choice2Count'] = 0
              el['choice3Count'] = 0
              el.replies &&
                Object.keys(el.replies).forEach(
                  student => el[`${el.replies[student]}Count`]++
                )
            })

            this.setState({ questions })
          })
        }
      })
  }

  render() {
    return (
      <div id="score">
        <NavbarComponent />
        <RedirectPage />

        <Tabs type="card" className="col-12 mt-4">
          {this.state.room.questions.map((question, index) => {
            if (!question.replies) {
              question.replies = { player: 'dumpAnswer' }
            }
            return (
              <TabPane tab={index + 1} key={index} className="row fadein">
                <div
                  className="row col-12 m-3 answer-title ml-auto mr-auto"
                  id="add"
                >
                  <div id="title--box">
                    <div className="unesco--box--bottom ">
                      <div className="unesco--box room--detail">
                        <div className="room">คำถาม</div>
                        <div>{question.questionName}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <Box className="container mx-auto mb-5">
                  <div className="row m-0 p-3 d-flex flex-column">
                    {question.answer}
                    <div className="amount title">{question.answerCount}</div>
                    <BorderBottom
                      width={`${+(
                        question['answerCount'] /
                        (this.state.room.students &&
                          Object.keys(this.state.room.students).length)
                      ) * 100 || 0}%`}
                      color="true"
                    />
                  </div>
                  <div className="row m-0 p-3 d-flex flex-column">
                    {question.choice1}
                    <div className="amount title">
                      {question['choice1Count']}
                    </div>
                    <BorderBottom
                      width={`${+(
                        question['choice1Count'] /
                        (this.state.room.students &&
                          Object.keys(this.state.room.students).length)
                      ) * 100 || 0}%`}
                    />
                  </div>
                  <div className="row m-0 p-3 d-flex flex-column">
                    {question.choice2}
                    <div className="amount title">
                      {question['choice2Count']}
                    </div>
                    <BorderBottom
                      width={`${+(
                        question['choice2Count'] /
                        (this.state.room.students &&
                          Object.keys(this.state.room.students).length)
                      ) * 100 || 0}%`}
                    />
                  </div>
                  <div className="row m-0 px-3 pt-3 d-flex flex-column">
                    {question.choice3}
                    <div className="amount title">
                      {question['choice3Count']}
                    </div>
                    <BorderBottom
                      width={`${+(
                        question['choice3Count'] /
                        (this.state.room.students &&
                          Object.keys(this.state.room.students).length)
                      ) * 100 || 0}%`}
                    />
                  </div>
                </Box>
              </TabPane>
            )
          })}
        </Tabs>
      </div>
    )
  }
}

export default ScorePage
