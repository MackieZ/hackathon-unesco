import React from 'react'
import NavbarComponent from '../Component/navbar'
import './AnswerPage.scss'
import { Tabs } from 'antd'
import * as firebase from 'firebase'
import Button from '../Component/his-button'
import * as moment from 'moment'
const TabPane = Tabs.TabPane

class AnswerPage extends React.Component {
  state = {
    questions: [],
    timer: 0,
    trigger: true,
    color: [
      'bg-color-red',
      'bg-color-green',
      'bg-color-blue',
      'bg-color-yellow',
      'bg-color-purple'
    ]
  }

  componentDidMount() {
    const { code } = this.props.location.state
    firebase
      .database()
      .ref('rooms/' + code)
      .once('value', data => {
        if (data.val()) {
          this.setState({ questions: data.val().questions })
        }
      })

    firebase
      .database()
      .ref('rooms/' + this.props.location.state.code)
      .on('value', snap => {
        const val = snap.val()
        const timeEnd = moment.unix(val.timeEnd)
        const duration = moment.duration(timeEnd.diff(moment()))

        this.setState(
          {
            timer: ~~duration.asSeconds() >= 0 ? ~~duration.asSeconds() : 0,
            ...val
          },
          () => this.state.trigger && this.timeCount()
        )
      })
  }

  timeCount = () => {
    this.setState(
      { trigger: false },
      () => (this.counter = setInterval(this.countDown, 1000))
    )
  }

  countDown = () => {
    const sec = this.state.timer - 1
    if (sec <= 0) {
      clearInterval(this.counter)
      this.setState(
        {
          timer: 0
        },
        () => {
          firebase
            .database()
            .ref('rooms/' + this.props.location.state.code + '/status')
            .set('done')
        }
      )
    } else {
      this.setState({
        timer: sec
      })
    }
  }

  handlerTimeStop = () => {
    const { code } = this.props.location.state
    firebase
      .database()
      .ref('rooms/' + code + '/status')
      .set('done')
      .then(() => {
        firebase
          .database()
          .ref('rooms/' + code)
          .once('value', snap => {
            const timeEnd = moment().unix('X')
            firebase
              .database()
              .ref('rooms/' + code + '/timeEnd')
              .set(timeEnd)
              .then(() => {
                this.setState({ timer: 0 })
              })
          })
      })
  }

  render() {
    return (
      <div>
        <NavbarComponent />

        <div className="timer" id="answer">
          {`${
            ~~(this.state.timer / 60) > 9
              ? `${~~(this.state.timer / 60)}`
              : `0${~~(this.state.timer / 60)}`
          } : ${
            ~~(this.state.timer % 60) <= 9
              ? `0${~~(this.state.timer % 60)}`
              : ~~(this.state.timer % 60)
          } `}
          <Button
            onClick={() => this.handlerTimeStop()}
            color="color-red"
            margin="8px"
          >
            หยุดเวลา
          </Button>
        </div>
        <Tabs type="card" className="col-12 mt-4">
          {this.state.questions.map((question, index) => {
            return (
              <TabPane tab={index + 1} key={index} className="row m-0 fadein">
                <div
                  className="row col-12 m-0 answer-title ml-auto mr-auto"
                  id="add"
                >
                  <div id="title--box">
                    <div className="unesco--box--bottom ">
                      <div className="unesco--box room--detail">
                        <div className="room">คำถาม</div>
                        <div>{question.questionName}</div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="answer" className="container px-5 pb-5">
                  {question.replies &&
                    Object.keys(question.replies).map((student, index) => {
                      return (
                        <div
                          style={{ width: 300, height: 'auto' }}
                          className={`${this.state.color[index % 5]} p-4`}
                        >
                          <div className="row m-0">{student}</div>

                          <div className="row m-0 mt-3">
                            {question.replies[student]}
                          </div>
                        </div>
                      )
                    })}
                </div>
              </TabPane>
            )
          })}
        </Tabs>
      </div>
    )
  }
}

export default AnswerPage
