import React, { Component } from 'react'
import './DashboardPage.scss'
import * as firebase from 'firebase'
import { Input, Icon } from 'antd'
import NavbarComponent from '../Component/navbar'
import RedirectPage from './RedirectPage'
import { Modal } from 'antd'
import { Tabs } from 'antd'
const TabPane = Tabs.TabPane

class DashboardPage extends Component {
  state = {
    roomName: '',
    roomDescription: '',
    visible: false,
    search: '',
    item: [],
    mode: 'choice'
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        firebase
          .database()
          .ref('rooms')
          .orderByChild('owner')
          .equalTo(user.uid)
          .once('value', snap => {
            const detail = snap.val()
            let item = []
            if (detail) {
              Object.keys(detail).map((val, index) => {
                item = [
                  ...item,
                  {
                    key: index,
                    status: detail[val].status,
                    name: detail[val].name,
                    description: detail[val].description,
                    code: detail[val].code,
                    mode: detail[val].mode
                  }
                ]
              })
            }
            this.setState({
              item: [...this.state.item, ...item]
            })
          })
      }
    })
  }

  handlerSearchInput = e => {
    this.setState({ search: e.target.value })
  }

  handlerItemClick = (key, box) => {
    const { status, name, description, mode } = box
    if (status === 'edit') {
      this.props.history.push({
        pathname: 'addquestion',
        state: { code: key }
      })
    } else if (status === 'waiting') {
      this.props.history.push({
        pathname: 'qrcode',
        state: {
          code: key,
          name,
          description
        }
      })
    } else if (status === 'active') {
      this.props.history.push({
        pathname: mode === 'choice' ? 'rank' : 'answer',
        state: {
          code: key
        }
      })
    } else {
      this.props.history.push({
        pathname: mode === 'choice' ? 'score' : 'answer',
        state: {
          code: key
        }
      })
    }
  }

  handlerRoomName = e => {
    this.setState({ roomName: e.target.value })
  }

  handlerRoomDescription = e => {
    this.setState({ roomDescription: e.target.value })
  }

  handleOk = () => {
    const { roomName, roomDescription, mode } = this.state
    this.props.history.push({
      pathname: '/addquestion',
      state: { roomName, roomDescription, mode }
    })
  }

  handleCancel = () => {
    this.setState({
      roomName: '',
      roomDescription: '',
      visible: false,
      mode: 'choice'
    })
  }

  addItemhandler = mode => {
    // this.props.history.push('/qrcode')
    // this.props.history.push('/addquestion')
    this.setState({ visible: true, mode })
  }

  render() {
    return (
      <div>
        <NavbarComponent activePath={'/dashboard'} />
        <RedirectPage />

        <div id="dashboard" className="row p-3 m-0 fadein">
          <div className="row input--item m-0">
            <Input
              className="border-input"
              placeholder="ค้นหาแบบทดสอบ"
              prefix={
                <Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />
              }
              value={this.state.search}
              onChange={this.handlerSearchInput}
            />
          </div>
          <Tabs type="card" className="col-12">
            <TabPane tab="ปรนัย" key="1" className="row m-0">
              <div
                className="box--item --add d-flex justify-content-center align-items-center"
                onClick={() => this.addItemhandler('choice')}
              >
                <div>
                  <div className="row icon m-0 d-flex justify-content-center">
                    <Icon type="plus" style={{ fontSize: 54 }} />
                  </div>
                  <div>เพิ่มแบบทดสอบปรนัย</div>
                </div>
              </div>
              {this.state.item.map(box => {
                if (
                  (!~box.name.indexOf(this.state.search) &&
                    !~box.description.indexOf(this.state.search)) ||
                  !~box.mode.indexOf('choice')
                )
                  return
                return (
                  <div
                    className="box--item"
                    onClick={() => this.handlerItemClick(box.code, box)}
                  >
                    <div className="box--detail d-flex align-items-center text-center justify-content-center">
                      {box.name}
                      <br />
                      ({box.description})
                    </div>
                    <div
                      className={`box--status d-flex justify-content-center ${
                        box.status === 'edit'
                          ? 'edit'
                          : box.status === 'waiting'
                            ? 'waiting'
                            : box.status === 'active'
                              ? 'active'
                              : 'done'
                      }`}
                    >
                      {box.status === 'edit'
                        ? 'ยังไม่เริ่มเกมส์'
                        : box.status === 'waiting'
                          ? 'กำลังรอเข้าเกมส์'
                          : box.status === 'active'
                            ? 'กำลังเริ่มเกมส์'
                            : 'เกมส์จบ'}
                    </div>
                  </div>
                )
              })}
            </TabPane>
            <TabPane tab="อัตนัย" key="2" className="row m-0">
              <div
                className="box--item --add d-flex justify-content-center align-items-center"
                onClick={() => this.addItemhandler('reply')}
              >
                <div>
                  <div className="row icon m-0 d-flex justify-content-center">
                    <Icon type="plus" style={{ fontSize: 54 }} />
                  </div>
                  <div>เพิ่มแบบทดสอบอัตนัย</div>
                </div>
              </div>
              {this.state.item.map(box => {
                if (
                  (!~box.name.indexOf(this.state.search) &&
                    !~box.description.indexOf(this.state.search)) ||
                  // ||
                  !~box.mode.indexOf('reply')
                )
                  return
                return (
                  <div
                    className="box--item"
                    onClick={() => this.handlerItemClick(box.code, box)}
                  >
                    <div className="box--detail d-flex align-items-center text-center justify-content-center">
                      {box.name}
                      <br />
                      ({box.description})
                    </div>
                    <div
                      className={`box--status d-flex justify-content-center ${
                        box.status === 'edit'
                          ? 'edit'
                          : box.status === 'waiting'
                            ? 'waiting'
                            : box.status === 'active'
                              ? 'active'
                              : 'done'
                      }`}
                    >
                      {box.status === 'edit'
                        ? 'ยังไม่เริ่มเกมส์'
                        : box.status === 'waiting'
                          ? 'กำลังรอเข้าเกมส์'
                          : box.status === 'active'
                            ? 'กำลังเริ่มเกมส์'
                            : 'เกมส์จบ'}
                    </div>
                  </div>
                )
              })}
            </TabPane>
          </Tabs>
        </div>
        <Modal
          title="สร้างห้องทำแบบทดสอบ"
          visible={this.state.visible}
          onCancel={() => this.handleCancel()}
          footer={[
            <button
              disabled={!this.state.roomDescription || !this.state.roomName}
              className="footer-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
              key="submit"
              type="submit"
              onClick={this.handleOk}
            >
              ตกลง
            </button>
          ]}
        >
          <div className="row m-0 mb-4 align-items-center">
            <div className="col-3 p-0 modal-body-title">ชื่อห้อง</div>
            <div className="col-9 p-0">
              <Input
                className="border-input"
                value={this.state.roomName}
                onChange={this.handlerRoomName}
              />
            </div>
          </div>
          <div className="row m-0 align-items-center">
            <div className="col-3 p-0 modal-body-title">รายละเอียด</div>
            <div className="col-9 p-0">
              <Input
                className="border-input"
                value={this.state.roomDescription}
                onChange={this.handlerRoomDescription}
              />
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}

export default DashboardPage
