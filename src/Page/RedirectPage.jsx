import React, { Component } from 'react';
import './LoginPage.scss';
import * as firebase from 'firebase';
import { Redirect } from 'react-router-dom'

class RedirectPage extends Component {
    state = {
        user: undefined
    }
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.setState({ user })
        })
    }

    render() {
        if (this.state.user === null) return <Redirect to='/login' />
        return <div></div>
        return (

            <div className="full-bg justify-content-center align-items-center">
                <h1>Loading ...</h1>
            </div>
        );
    }
}

export default RedirectPage;
