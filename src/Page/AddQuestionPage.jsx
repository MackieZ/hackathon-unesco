import React, { Component } from 'react'
import './AddQuestionPage.scss'
import * as firebase from 'firebase'
import { Input, Icon, Row, Col, message } from 'antd'
import NavbarComponent from '../Component/navbar'
import RedirectPage from './RedirectPage'

class AddQuestionPage extends Component {
  state = {
    questions: [],
    time: 0,
    code: '',
    name: '',
    description: '',
    status: 'edit',
    mode: 'choice'
  }

  componentDidMount() {
    const { roomName, roomDescription, mode } = this.props.location.state
    this.setState({ name: roomName, description: roomDescription }, () => {
      const roomCode = this.props.location.state.code || ''
      if (roomCode) {
        console.log(roomCode)
        firebase
          .database()
          .ref('rooms/' + roomCode)
          .once('value')
          .then(snapshot => {
            if (snapshot) {
              const {
                code,
                description,
                name,
                questions,
                status,
                time,
                mode
              } = snapshot.val()
              this.setState({
                code,
                description,
                name,
                status,
                time: time,
                mode,
                questions: questions || this.state.questions
              })
            }
          })
      } else {
        firebase
          .database()
          .ref('rooms')
          .once('value')
          .then(snapshot => {
            let en = true
            while (en) {
              const str = Math.random()
                .toString(36)
                .replace(/[^a-zA-Z0-9]+/g, '')
                .substr(1, 6)
              if (!snapshot.val()[str]) {
                this.setState({ code: str, mode }, () => {
                  this.addField()
                })
                en = false
              }
            }
          })
      }
    })
  }

  handlerItemClick = key => {
    this.props.history.push('/rank')
  }

  addItemhandler = () => {
    this.props.history.push('/qrcode')
  }

  handlerQuestionInput = (e, ind, fieldName) => {
    const q = this.state.questions
    q[ind][fieldName] = e.target.value
    this.setState({
      questions: q
    })
  }

  handlerTime = e => {
    this.setState({
      time: e.target.value
    })
  }

  addField = () => {
    const q = this.state.questions
    this.state.mode === 'choice'
      ? this.setState({
          questions: [
            ...q,
            {
              questionName: '',
              answer: '',
              choice1: '',
              choice2: '',
              choice3: ''
            }
          ]
        })
      : this.setState({
          questions: [
            ...q,
            {
              questionName: ''
            }
          ]
        })
  }

  removeField = ind => {
    const q = this.state.questions.filter((question, index) => index !== ind)
    this.setState({
      questions: q
    })
  }

  saveQuestion = e => {
    let ret = false
    this.state.questions.forEach(question => {
      Object.keys(question).map(q => {
        if (!question[q]) {
          ret = true
        }
      })
    })
    if (ret) {
      return message.info('กรุณาใส่คำถามและคำตอบให้ครบถ้วน')
    }

    firebase
      .database()
      .ref('rooms/' + this.state.code)
      .set({
        ...this.state,
        time: this.state.time,
        owner: firebase.auth().currentUser.uid
      })
      .then(() => {
        message.success('บันทึกคำถามเสร็จสิ้น')
      })
      .catch(() => {
        message.error('เกิดข้อผิดพลาดทางอินเทอร์เน็ต')
      })
  }

  confirmQuestion = e => {
    let ret = false
    this.state.questions.forEach(question => {
      Object.keys(question).map(q => {
        if (!question[q]) {
          ret = true
        }
      })
    })
    if (ret) {
      return message.info('กรุณาใส่คำถามและคำตอบให้ครบถ้วน')
    }

    this.setState(
      {
        status: 'waiting'
      },
      () => {
        firebase
          .database()
          .ref('rooms/' + this.state.code)
          .set({
            ...this.state,
            time: this.state.time,
            owner: firebase.auth().currentUser.uid
          })
          .then(() => {
            this.props.history.push({
              pathname: 'qrcode',
              state: {
                name: this.state.name,
                description: this.state.description,
                code: this.state.code,
                mode: this.state.mode
              }
            })
          })
          .catch(() => {
            message.error('เกิดข้อผิดพลาดทางอินเทอร์เน็ต')
          })
      }
    )
  }

  render() {
    const { name, description } = this.state
    return (
      <div className="pb-5">
        <NavbarComponent activePath={'/AddQuestion'} />
        <RedirectPage />
        <div id="add" className="fadein">
          <Row id="title--box">
            <div className="unesco--box--bottom ">
              <div className="unesco--box room--detail">
                <div className="room">ROOM</div>
                <div>
                  {name ? name : 'ผู้คนและพื้นที่'} ({description
                    ? description
                    : !name && 'บ้านในพื้นที่สูง : Honai and tongkonan'})
                </div>
              </div>
            </div>
          </Row>
          {this.state.questions.map((question, index) => {
            return (
              <div className="question--box" key={index}>
                <Row
                  className="row input--item"
                  id={this.state.mode === 'choice' && 'underline'}
                >
                  <Col md={4}>
                    <div className="row m-0 d-flex align-items-center">
                      <div className="circle d-flex justify-content-center align-items-center">
                        {index + 1}
                      </div>
                      <span className="pl-2">คำถาม : </span>
                    </div>
                  </Col>
                  <Col md={20}>
                    <Input
                      className="input--item"
                      value={this.state.questions[index].questionName}
                      onChange={e =>
                        this.handlerQuestionInput(e, index, 'questionName')
                      }
                    />
                  </Col>
                  <div
                    className="close-btn"
                    onClick={() => this.removeField(index)}
                  >
                    x
                  </div>
                </Row>
                {this.state.mode === 'choice' && (
                  <div>
                    <Row className="row align-items-center d-flex input--item">
                      <Col md={4}>
                        <span id="answer">เฉลย : </span>
                      </Col>
                      <Col md={20}>
                        <Input
                          value={this.state.questions[index].answer}
                          onChange={e =>
                            this.handlerQuestionInput(e, index, 'answer')
                          }
                        />
                      </Col>
                    </Row>
                    <Row className="row align-items-center d-flex input--item">
                      <Col md={4}>
                        <span>ตัวเลือกอื่นๆ (1) : </span>
                      </Col>
                      <Col md={20}>
                        <Input
                          value={this.state.questions[index].choice1}
                          onChange={e =>
                            this.handlerQuestionInput(e, index, 'choice1')
                          }
                        />
                      </Col>
                    </Row>{' '}
                    <Row className="row align-items-center d-flex input--item">
                      <Col md={4}>
                        <span>ตัวเลือกอื่นๆ (2) : </span>
                      </Col>
                      <Col md={20}>
                        <Input
                          value={this.state.questions[index].choice2}
                          onChange={e =>
                            this.handlerQuestionInput(e, index, 'choice2')
                          }
                        />
                      </Col>
                    </Row>
                    <Row className="row align-items-center d-flex input--item">
                      <Col md={4}>
                        <span>ตัวเลือกอื่นๆ (3) : </span>
                      </Col>
                      <Col md={20}>
                        <Input
                          value={this.state.questions[index].choice3}
                          onChange={e =>
                            this.handlerQuestionInput(e, index, 'choice3')
                          }
                        />
                      </Col>
                    </Row>
                  </div>
                )}
              </div>
            )
          })}
        </div>

        <Row>
          <div
            id="add--question"
            className="--box --add d-flex justify-content-center align-items-center"
            onClick={this.addField}
          >
            <Icon className="pr-2" type="plus" style={{ fontSize: 20 }} />
            เพิ่มคำถาม
          </div>
        </Row>

        <div className="add-question-row p-3">
          <div className="row">
            <div id="time">
              <Col md={4}>
                <Icon
                  className="pr-2 red"
                  type="caret-right"
                  style={{ fontSize: 20, color: '#ff7e7b' }}
                />เวลาที่กำหนด
              </Col>
              <Col md={20}>
                <Input
                  min="0"
                  id="error"
                  type="number"
                  className="input--item"
                  value={this.state.time}
                  onChange={this.handlerTime}
                />
                <span className="pl-4">นาที</span>
              </Col>
            </div>
          </div>
        </div>

        <Row>
          <div id="btn" className=" d-flex justify-content-end">
            <button
              disabled={
                this.state.time <= 0 || this.state.questions.length === 0
              }
              id="save"
              className=" mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
              onClick={this.saveQuestion}
            >
              บันทึก
            </button>
            <button
              disabled={
                this.state.time <= 0 || this.state.questions.length === 0
              }
              id="start"
              className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
              onClick={this.confirmQuestion}
            >
              ยืนยัน
            </button>
          </div>
        </Row>
      </div>
    )
  }
}

export default AddQuestionPage
