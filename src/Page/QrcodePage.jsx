import React, { Component } from 'react'
import './QrcodePage.scss'
import * as firebase from 'firebase'
import NavbarComponent from '../Component/navbar'
import RedirectPage from './RedirectPage'
import QRCode from 'qrcode.react'
import moment from 'moment'
import Button from '../Component/his-button'

class QrcodePage extends Component {
  state = {
    studentsCount: 0,
    students: {},
    color: [
      'color-red',
      'color-yellow',
      'color-green',
      'color-purple',
      'color-grey',
      'color-blue',
      'color-light-black'
    ]
  }

  componentDidMount() {
    firebase
      .database()
      .ref('rooms/' + this.props.location.state.code + '/students')
      .on('value', snap => {
        this.setState({
          studentsCount: (snap.val() && Object.keys(snap.val()).length) || 0,
          students: snap.val() || this.state.students
        })
      })
  }

  handlerButtonClick = () => {
    const { code, mode } = this.props.location.state
    firebase
      .database()
      .ref('rooms/' + code)
      .once('value', snap => {
        const val = snap.val()
        const timeEnd = moment()
          .add(+val.time, 'minutes')
          .unix('X')
        firebase
          .database()
          .ref('rooms/' + code + '/timeEnd')
          .set(timeEnd)
          .then(() => {
            fetch(
              `https://us-central1-hackathon-unesco-2018.cloudfunctions.net/timerRoom?room=${code}`
            )
          })
        firebase
          .database()
          .ref('rooms/' + code + '/status')
          .set('active')
      })
    this.props.history.push({
      pathname: mode === 'choice' ? 'rank' : 'answer',
      state: { code }
    })
  }

  render() {
    const { code, name, description } = this.props.location.state || ''
    return (
      <div>
        <NavbarComponent activePath={'/dashboard'} />
        <RedirectPage />
        <div
          id="qrcode"
          className="row d-flex justify-content-center p-3 m-0 fadein"
        >
          <div className="row qr d-flex m-0 p-3 justify-content-center align-items-center">
            <QRCode value={code ? code : 'UNESCO'} size={192} />
          </div>
          <div className="col-12 m-0 p-3 title text-center">
            จำนวนนักเรียน :{' '}
            <span className="color-purple" style={{ fontSize: '36px' }}>
              {this.state.studentsCount}
            </span>
          </div>
          <div className="student-box title">
            <div className="row m-0">Waiting ...</div>
            {Object.keys(this.state.students).map(student => {
              return (
                <div className="row m-0 popup">
                  <Button
                    boxShadow="none"
                    color={`${this.state.color[~~(Math.random() * 6)]}`}
                    margin="8px"
                  >
                    {this.state.students[student].name}
                  </Button>
                </div>
              )
            })}
          </div>
          <div className="unesco--box--bottom ">
            <div className="qr--detail unesco--box text-center">
              <div className="qr--room">
                รหัสห้อง :{' '}
                <span className="room">{code ? code : 'UNESCO'}</span>
              </div>
              <div className="room--name">
                {name} ({description} ){' '}
              </div>
            </div>
          </div>
          <div className="row col-12 p-3 m-0 d-flex justify-content-center align-items-center">
            <button
              onClick={this.handlerButtonClick}
              className="start-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
            >
              เริ่มเกมส์
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default QrcodePage
