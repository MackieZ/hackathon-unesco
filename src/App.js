import React, { Component } from 'react'
import './App.scss'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import * as firebase from 'firebase'
import RedirectPage from './Page/RedirectPage'
import LoginPage from './Page/LoginPage'
import logo from './assets/image/logo-text.png'
import DashboardPage from './Page/DashboardPage'
import { Redirect } from 'react-router-dom'
import QrcodePage from './Page/QrcodePage'
import RankPage from './Page/RankPage'
import AddQuestionPage from './Page/AddQuestionPage'
import AnswerPage from './Page/AnswerPage'
import DocumentPage from './Page/DocumentPage'
import ScorePage from './Page/ScorePage'

class App extends Component {
  state = {
    user: null
  }

  constructor() {
    super()
    // Initialize Firebase
    var config = {
      apiKey: 'AIzaSyDKVdjnvxYYXWC1E9CpDJ7iDmzYlWVzj9Y',
      authDomain: 'hackathon-unesco-2018.firebaseapp.com',
      databaseURL: 'https://hackathon-unesco-2018.firebaseio.com',
      projectId: 'hackathon-unesco-2018',
      storageBucket: 'hackathon-unesco-2018.appspot.com',
      messagingSenderId: '535156069753'
    }
    firebase.initializeApp(config)
  }

  componentDidMount() {}

  render() {
    return (
      <Router>
        <Switch>
          <div className="App">
            <Route
              exact
              path="/"
              render={() => {
                return <Redirect to="/dashboard" />
              }}
            />
            <Route path="/login" component={LoginPage} />
            <Route path="/dashboard" component={DashboardPage} />
            <Route path="/qrcode" component={QrcodePage} />
            <Route path="/rank" component={RankPage} />
            <Route path="/addquestion" component={AddQuestionPage} />
            <Route path="/answer" component={AnswerPage} />
            <Route path="/document" component={DocumentPage} />
            <Route path="/score" component={ScorePage} />
          </div>
        </Switch>
      </Router>
    )
  }
}

export default App
