import React, { Component } from 'react'
import * as firebase from 'firebase'
import logo from '../assets/image/logo-text.png'
import './navbar.scss'
import { Redirect, withRouter } from 'react-router-dom'

class NavbarComponent extends Component {
  state = {
    user: null,
    loading: true
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({ user, loading: false })
    })
  }

  handlerSignout = () => firebase.auth().signOut()

  render() {
    if (this.state.loading)
      return (
        <div className="loader-bg">
          <div className="loader" />
        </div>
      )
    return (
      <div>
        {this.state.user ? (
          <div className="row nav-bar d-flex align-items-center">
            <img alt="histolearn-logo" src={logo} className="nav--logo" />
            <div
              onClick={() => {
                this.props.history.push('/dashboard')
              }}
              className={`menu--item ${
                this.props.activePath === '/dashboard' ? 'active' : ''
              }`}
            >
              ห้องเรียน
            </div>
            <div
              onClick={() => {
                this.props.history.push('document')
              }}
              className={`menu--item ${
                this.props.activePath === '/download' ? 'active' : ''
              }`}
            >
              ดาวน์โหลดเอกสาร
            </div>
            <div onClick={this.handlerSignout} className="menu--item logout">
              ออกจากระบบ
            </div>
          </div>
        ) : (
          <div className="row nav-bar d-flex fix align-items-center">
            เข้าสู่ระบบ
          </div>
        )}
      </div>
    )
  }
}

export default withRouter(NavbarComponent)
