import React from 'react'
import styled from 'styled-components'

const Button = styled.button`
  margin: ${props => props.margin} !important;
  padding: ${props => props.padding} !important;
  width: ${props => props.width} !important;
  border-radius: 24px !important;
  &.mdl-button--raised {
    box-shadow: ${props => props.boxShadow};
  }
  max-width: ${props => props.maxWidth} !important;
  font-size: ${props => props.fontSize || '20px'} !important;
  &.mdl-button--accent.mdl-button--accent.mdl-button--raised {
    background-color: ${props =>
      props.color === 'color-red'
        ? '#ff7e7b'
        : props.color === 'color-red'
          ? '#ff7e7b'
          : props.color === 'color-yellow'
            ? '#ffb84e'
            : props.color === 'color-green'
              ? '#57e06e'
              : props.color === 'color-purple'
                ? '#9f8eed'
                : props.color === 'color-black'
                  ? '#4f4f4f'
                  : props.color === 'color-grey'
                    ? '#c6c6c6'
                    : props.color === 'color-light-black'
                      ? '#7c7c7c'
                      : props.color === 'color-blue'
                        ? '#50b2f'
                        : '#ff7e7b'};
  }
`

export default props => (
  <Button
    boxShadow={props.boxShadow}
    maxWidth={props.maxWidth}
    fontSize={props.fontSize}
    color={props.color}
    margin={props.margin}
    padding={props.padding}
    width={props.width}
    onClick={props.onClick}
    className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
  >
    {props.children}
  </Button>
)
